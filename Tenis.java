/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Blog.es.esy.gustavoti;

/**
 *
 * @author 181510004
 */
public class Tenis {
        private int numero;
        private String cor;
        private double valor;

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public Tenis(int numero, String cor, double valor) {
        this.numero = numero;
        this.cor = cor;
        this.valor = valor;
    }
        
        
        
}
