/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Blog.es.esy.gustavoti;

import java.awt.HeadlessException;
import javax.swing.JOptionPane;

/**
 *
 * @author 181510004
 */
public class Vendas {

    
    static Tenis vTenis[] = new Tenis[20];
    static String cores[]={"Branca", "Preta", "Prata", "Azul", "Vermelha", "Verde"};
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        menu();
    }

    private static void menu() {
        String menu[] = new String[]{
            "1 - Cadastra Tenis",
            "2 - Lista tenis",
            "3 - Lista por números",
            "4 - Lista por cor"
        };
        switch(lerMenu(menu)){
            case 1: cadastra(); menu(); break;
            case 2: listaTodos(); menu(); break;
            case 3: listaNumeros(); menu(); break;
            case 4: listaCor();menu();  break;
            default: escreva("Tente novamente! Digite um número valido!"); menu(); break;
        }
    }

    private static int lerMenu(Object[] menu) {
        int opcao = 0;
        try{
            opcao = JOptionPane.showOptionDialog(null, "Escolha:", "Menu", 0, JOptionPane.PLAIN_MESSAGE, null, menu, menu);
            if(opcao == 0){opcao = 1;}
            else if(opcao == 1){opcao = 2;}
            else if(opcao == 2){opcao = 3;}
            else if(opcao == 3){opcao = 4;}
            else { opcao = 0; }

        }catch(HeadlessException e){
            System.out.println(e.getMessage());
        }
        return opcao;
    }

    private static void listaCor() {
        
    }

    private static void listaNumeros() {
        
    }

    private static void listaTodos() {
        
    }

    private static void cadastra() {
      
    }

    private static void escreva(String texto) {
        JOptionPane.showMessageDialog(null, texto);
    }
    
}
